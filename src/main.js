import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'
// import Vuetify from 'vuetify'
// import VueCarousel from 'vue-carousel'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

/* import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css' */

import routes from './routes.js'
import store from './store/store.js'

// Vue.use(BootstrapVue)

Vue.use(VueRouter)
// Vue.use(Vuetify)
// Vue.use(VueCarousel)

Vue.use(VueAwesomeSwiper)
Vue.use(Buefy)

const router = new VueRouter({
  routes,
  mode: 'history'
})
new Vue({
  el: '#app',
  render: h => h(App),
  router,
  store
})
