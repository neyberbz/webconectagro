import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    isAuthenticated: false,
    hasStore: false,
    userId: 1,
    storeId: 0
  },
  mutations: {
    login (state, id) {
      state.isAuthenticated = true
      state.userId = id
    },
    logout (state) {
      state.isAuthenticated = false
      state.userId = 1
      state.storeId = 0
    },
    store (state, id) {
      state.storeId = id
    }
  }
})

export default store
