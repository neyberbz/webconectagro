import Home from './components/Home'
import Login from './components/Login'
import Register from './components/Register'
import ProductDetail from './components/ProductDetail'
import StoreRegister from './components/StoreRegister'
import Help from './components/Help'
import Products from './components/Products'
import Sales from './components/Sales'
import Purchases from './components/Purchases'
import Favorites from './components/Favorites'
import Notifications from './components/Notifications'
import RecoverPassword from './components/RecoverPassword'
import ProductCart from './components/ProductCart'
import ProductRegister from './components/ProductRegister'

const routes = [
  {
    path: '/',
    component: Home,
    name: '/'
  },
  {
    path: '/login',
    component: Login,
    name: 'login'
  },
  {
    path: '/registro',
    component: Register,
    name: 'register'
  },
  {
    path: '/producto/:id',
    component: ProductDetail,
    name: 'product'
  },
  {
    path: '/registro-tienda',
    component: StoreRegister,
    name: 'store-register'
  },
  {
    path: '/ayuda',
    component: Help,
    name: 'help'
  },
  {
    path: '/mis-productos',
    component: Products,
    name: 'products'
  },
  {
    path: '/mis-ventas',
    component: Sales,
    name: 'sales'
  },
  {
    path: '/mis-compras',
    component: Purchases,
    name: 'purchases'
  },
  {
    path: '/mis-favoritos',
    component: Favorites,
    name: 'favorites'
  },
  {
    path: '/mis-notificaciones',
    component: Notifications,
    name: 'notifications'
  },
  {
    path: '/recuperar-contrasena',
    component: RecoverPassword,
    name: 'recover-password'
  },
  {
    path: '/carrito-compras',
    component: ProductCart,
    name: 'product-cart'
  },
  {
    path: '/registro-producto',
    component: ProductRegister,
    name: 'product-register'
  }
]

export default routes
